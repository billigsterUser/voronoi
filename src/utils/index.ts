import { RndFn } from '../core/main.js'
import { IPoint } from '../core/model.js'

export function getRndInt(min: number, max: number) {
	return Math.ceil(Math.random() * (max - min) + min)
}
export function genRndSeeds2(seedCount: number, height: number, width: number) {
	const seeds = new Array<IPoint>(seedCount)
	for (let i = 0; i < seedCount; i++) {
		seeds[i] = {
			x: getRndInt(0, width),
			y: getRndInt(0, height)
		}
	}
	return seeds
}
export function genRndSeeds(rndFn: RndFn, seedCount: number, height: number, width: number) {
	const seeds = new Array<IPoint>(seedCount)
	for (let i = 0; i < seedCount; i++) {
		seeds[i] = {
			x: rndFn(0, width),
			y: rndFn(0, height)
		}
	}
	return seeds
}
export function fillCircel(arr: Uint32Array[], p: IPoint, radius: number, c: number) {
	const width = arr[0].length
	const height = arr.length
	const x0 = p.x - radius
	const y0 = p.y - radius
	const x1 = p.x + radius
	const y1 = p.y + radius
	for (let x = x0; x <= x1; x++) {
		if (x < 0 || x >= width) { continue }
		for (let y = y0; y <= y1; y++) {
			if (y < 0 || y >= height) { continue }
			if (sqrDist(p, { x, y }) > radius * radius || arr[y][x] === c) { continue }
			arr[y][x] = c
		}
	}
	return arr
}
export function sqrDist(p: IPoint, p1: IPoint) {
	const dx = p.x - p1.x
	const dy = p.y - p1.y
	return dx * dx + dy * dy
}
export function minkowskiDistance(p: IPoint, p1: IPoint, o= 3) {
	let d = 0
	d += Math.pow(Math.abs(p.x - p1.x), o)
	d += Math.pow(Math.abs(p.y - p1.y), o)
	return Math.pow(d, 1 / o)
}
export function manhattanDistance(p: IPoint, p1: IPoint) {
	return Math.abs(p1.x - p.x) + Math.abs(p1.y - p.y)
}
export function euclideanDistance(p: IPoint, p1: IPoint) {
	const dx = p.x - p1.x
	const dy = p.y - p1.y
	return Math.sqrt(dx * dx + dy * dy)
}
export function chebyshevDistance(p: IPoint, p1: IPoint) {
	const max = Math.abs(p.x - p1.x)
	const distance = Math.abs(p.y - p1.y)
	return distance > max ? distance : max
}
export function makeArr(height: number, width: number, fill?: number) {
	return typeof fill === 'undefined'
		? Array.from({ length: height }, () => new Uint32Array(width))
		: Array.from({ length: height }, () => new Uint32Array(width).fill(fill))
}
export function fillArr(arr: Uint32Array[], c?: number) {
	const width = arr[0].length
	const height = arr.length
	for (let i = 0; i < height; ++i) {
		for (let j = 0; j < width; ++j) {
			if (!c) { arr[i][j] = getRndInt(0, 0xFFFFFFFF) } else { arr[i][j] = c }
		}
	}
	return arr
}
export function createArr(height: number, width: number, c?: number) {
	const arr = new Array<Uint32Array>(height)
	for (let i = 0; i < height; ++i) {
		const columns = new Uint32Array(width)
		for (let j = 0; j < width; ++j) {
			if (!c) { columns[j] = getRndInt(0, 0xFFFFFFFF) } else { columns[j] = c }
		}
		arr[i] = columns
	}
	return arr
}
export function toByteArr(arr: Uint32Array[]) {
	const width = arr[0].length
	const height = arr.length
	let count = -1
	const r = new Uint8Array(height * width * 3)
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			const dv = new DataView(new ArrayBuffer(4))
			dv.setUint32(0, arr[y][x], true)
			const bytes = new Uint8Array([
				dv.getUint8(0),
				dv.getUint8(1),
				dv.getUint8(2)
			])
			for (const byte of bytes) {
				count++
				r[count] = byte
			}
		}
	}
	return r
}




export function renderGradient(arr: Uint32Array[]) {
	const width = arr[0].length
	const height = arr.length
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			arr[y][x] = pointToColor({ x, y })
		}
	}
	return arr
}
export function pointToColor(p: IPoint) {
	const dv = new DataView(new ArrayBuffer(4))
	dv.setUint16(0, p.x)
	dv.setUint16(2, p.y)
	return dv.getUint32(0)
}