import { PathOrFileDescriptor, writeFile } from 'fs'

function buildHeader(width: number, height: number) {
	return new Uint8Array(Buffer.from(`P6\n${width} ${height} 255\n`))
}
export function createPpmFile(
	filename: PathOrFileDescriptor,
	imageData: Uint8Array,
	width: number,
	height: number
) {
	return new Promise<void>((resolve, reject) => {
		const header = buildHeader(width, height)
		const fileContent = new Uint8Array(header.byteLength + (height * width * 3))
		fileContent.set(header)
		fileContent.set(imageData, header.byteLength)

		writeFile(filename, fileContent, err => {
			if (err) { return reject(err) }
			resolve()
		})
	})
}