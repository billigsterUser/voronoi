import { PathOrFileDescriptor, writeFile } from 'fs'

function bitmapFileHeader({
	filesize = 0,
	applicationHeader = 0,
	imageDataOffset = 0
}) {
	const dv = new DataView(new ArrayBuffer(14))
	// A bitmap file starts with a "BM" in ASCII.
	dv.setUint8(0, 'B'.charCodeAt(0))
	dv.setUint8(1, 'M'.charCodeAt(0))
	// The entire filesize.
	dv.setInt32(2, filesize, true)
	// 4 bytes reserved for the application creating the image.
	dv.setInt32(6, applicationHeader, true)
	// The byte offset to access the pixel data.
	dv.setInt32(10, imageDataOffset, true)
	return new Uint8Array(dv.buffer)
}
// Creates a DIB header, specifically a BITMAPINFOHEADER type
// since it's the most widely supported.
function dibHeader(
	width: number,
	height: number,
	bitsPerPixel: number,
	bitmapDataSize: number,
	numberOfColorsInPalette: number
) {
	const dv = new DataView(new ArrayBuffer(40))
	// The size of the header.
	dv.setInt32(0, 40, true)
	// The width and height of the bitmap image.
	dv.setInt32(4, width, true)
	dv.setInt32(8, height, true)
	// The number of color planes, which in bitmap files is always 1
	dv.setInt16(12, 1, true)
	dv.setInt16(14, bitsPerPixel, true)

	// Compression method, not supported in this package.
	dv.setInt32(16, 0, true)
	dv.setInt32(20, bitmapDataSize, true)
	// The horizontal and vertical resolution of the image.
	// On monitors: 72 DPI × 39.3701 inches per metre yields 2834.6472
	dv.setInt32(24, 2835, true)
	dv.setInt32(28, 2835, true)
	// Number of colors in the palette.
	dv.setInt32(32, numberOfColorsInPalette, true)
	// Number of important colors used.
	dv.setInt32(36, 0, true)
	return new Uint8Array(dv.buffer)
}
export function createBitmapFile(
	filename: PathOrFileDescriptor,
	imageData: Uint8Array,
	width: number,
	height: number,
	bitsPerPixel: number,
	colorTable = new Uint8Array(0)
) {
	return new Promise<void>((resolve, reject) => {
		// imageData = padImageData(imageData, width, height)
		const imageDataOffset = 54 + colorTable.length
		const filesize = imageDataOffset + imageData.length
		const fileContent = new Uint8Array(filesize)
		const fileHeader = bitmapFileHeader({
			filesize,
			imageDataOffset
		})
		fileContent.set(fileHeader)
		fileContent.set(dibHeader(
			width,
			height,
			bitsPerPixel,
			imageData.length,
			colorTable.length / 4
		), 14)

		fileContent.set(colorTable, 54)

		fileContent.set(imageData, imageDataOffset)

		writeFile(filename, fileContent, err => {
			if (err) { return reject(err) }
			resolve()
		})
	})
}