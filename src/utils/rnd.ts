export function buldRndIntGen(fn: () => number) {
	return (min: number, max: number) => Math.floor(fn() * (max - min) + min)
}
export function mulberry32(a: number) {
	return () => {
		let t = a += 0x6D2B79F5
		// tslint:disable-next-line: no-bitwise
		t = Math.imul(t ^ t >>> 15, t | 1)
		// tslint:disable-next-line: no-bitwise
		t ^= t + Math.imul(t ^ t >>> 7, t | 61)
		// tslint:disable-next-line: no-bitwise
		return ((t ^ t >>> 14) >>> 0) / 4294967296
	}
}
/* export function getRndInt(min: number, max: number) {
	return Math.floor(Math.random() * (max - min) + min)
} */

export const seededRndIntGen = buldRndIntGen(mulberry32(1))
export const rndIntGen = buldRndIntGen(Math.random)


