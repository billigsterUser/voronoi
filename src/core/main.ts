import { createBitmapFile } from '../utils/bitmap.js'
import {
	chebyshevDistance, euclideanDistance, fillCircel,
	genRndSeeds, makeArr, manhattanDistance, minkowskiDistance, toByteArr
} from '../utils/index.js'
import { createPpmFile } from '../utils/ppm.js'
import { buldRndIntGen, mulberry32 } from '../utils/rnd.js'
import { renderVoronoi } from './index.js'
import { IPoint } from './model.js'
import { color } from './palette.js'

export type RndFn = (min: number, max: number) => number
export type DistCalcFn = (p: IPoint, p1: IPoint) => number
export interface IConfig {
	distCalcFn: DistCalcFn
	filename: string
	height: number
	rndFn: RndFn
	seedMarker: {
		color: typeof color[keyof typeof color]
		count: number
		radius: number
	}
	width: number

}
export async function main({
	width, height, rndFn, distCalcFn, filename, seedMarker: {
		count, color: seedColor, radius
	}
}: IConfig) {
	const arr = makeArr(height, width)
	const seeds = genRndSeeds(rndFn, count, height, width)
	renderVoronoi(distCalcFn, arr, seeds)
	for (const seed of seeds) { fillCircel(arr, seed, radius, seedColor) }
	const uint8Arr = toByteArr(arr)
	await Promise.all([
		createPpmFile(`data/${filename}.ppm`, uint8Arr, width, height),
		createBitmapFile(`data/${filename}.bmp`, uint8Arr, width, height, 24)
	])
}
async function genAll() {
	const fns = [minkowskiDistance, chebyshevDistance, manhattanDistance, euclideanDistance]
	for (const fn of fns) {
		// eslint-disable-next-line no-await-in-loop
		await main({
			distCalcFn: fn,
			filename: `seededRnd-${fn.name}`,
			height: 600,
			rndFn: buldRndIntGen(mulberry32(1)),
			seedMarker: {
				color: color.gray,
				count: 10,
				radius: 5,
			},
			width: 800,
		})
	}
}
await genAll()