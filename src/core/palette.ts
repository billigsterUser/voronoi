import { randomBytes } from 'crypto'

export const color = {
	red: 0xFF0000FF,
	green: 0xFF00FF00,
	blue: 0xFFFF0000,
	white: 0xFFFFFFFF,
	black: 0xFF000000,
	gray: 0xFF181818,

	brightRed: 0xFF3449FB,
	brightGreen: 0xFF26BBB8,
	brightYellow: 0xFF2FBDFA,
	brightBlue: 0xFF98A583,
	brightPurple: 0xFF9B86D3,
	brightAqua: 0xFF7CC08E,
	brightOrange: 0xFF1980FE,
} as const
export const palette = [...Object.values(color)] as const

export function genRndColorPalette(len: number, uniq = true) {
	const arr = new Uint32Array(len)
	for (let i = 0; i < arr.length; i++) {
		let c = genColor()
		if (uniq) { while (arr.includes(c)) { c = genColor() } }
		arr[i] = c
	}
}
export function genColor() {
	return parseInt(`0xFF${randomBytes(6).toString('hex')}`, 16)
}