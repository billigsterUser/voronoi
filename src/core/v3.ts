import { createBitmapFile } from '../utils/bitmap.js'
import { fillCircel, genRndSeeds2, makeArr, manhattanDistance, toByteArr } from '../utils/index.js'
import { createPpmFile } from '../utils/ppm.js'
import { IPoint } from './model.js'
import { color, genColor } from './palette.js'

function applyNextSeed(arr: Uint32Array[], depthArr: Uint32Array[], seeds: IPoint[], idx: number) {
	const width = arr[0].length
	const height = arr.length

	const p = seeds[idx]
	const c = genColor() // palette[idx % palette.length]

	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			const d = manhattanDistance(p, { x, y })
			if (d > depthArr[y][x]) { continue }
			depthArr[y][x] = d
			arr[y][x] = c
		}
	}
	return arr
}
function renderVoronoi3(arr: Uint32Array[], depthArr: Uint32Array[], seeds: IPoint[]) {
	for (let i = 0; i < seeds.length; i++) { applyNextSeed(arr, depthArr, seeds, i) }
}
export async function main() {
	const width = 800
	const height = 600
	const seedCount = 20

	let arr = makeArr(height, width)
	const depthArr = makeArr(height, width, 4294967295) // max value

	const seeds = genRndSeeds2(seedCount, height, width)

	renderVoronoi3(arr, depthArr, seeds)
	for (const seed of seeds) { arr = fillCircel(arr, seed, 5, color.gray) }

	const uint8Arr = toByteArr(arr)

	await Promise.all([
		createPpmFile('3.ppm', uint8Arr, width, height),
		createBitmapFile('3.bmp', uint8Arr, width, height, 24)
	])
}
await main()