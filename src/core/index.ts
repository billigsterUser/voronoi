import { createBitmapFile } from '../utils/bitmap.js'
import { euclideanDistance, fillCircel, genRndSeeds2, makeArr, toByteArr } from '../utils/index.js'
import { createPpmFile } from '../utils/ppm.js'
import { DistCalcFn } from './main.js'
import { IPoint } from './model.js'
import { color, palette } from './palette.js'
export function renderVoronoi(distCalcFn: DistCalcFn, arr: Uint32Array[], seeds: IPoint[]) {
	const width = arr[0].length
	const height = arr.length
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			let j = 0
			for (let i = 1; i < seeds.length; i++) {
				if (distCalcFn(seeds[i], { x, y }) > distCalcFn(seeds[j], { x, y })) { continue }
				j = i
			}
			arr[y][x] = palette[j % palette.length]
		}
	}
	return arr
}
function renderVoronoi2(arr: Uint32Array[], seeds: IPoint[]) {
	const width = arr[0].length
	const height = arr.length
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			let j = 0
			for (let i = 1; i < seeds.length; i++) {
				if (euclideanDistance(seeds[i], { x, y }) > euclideanDistance(seeds[j], { x, y })) { continue }
				j = i
			}
			arr[y][x] = palette[j % palette.length]
		}
	}
	return arr
}
export async function main() {
	const width = 800
	const height = 600
	let arr = makeArr(height, width)
	const seeds = genRndSeeds2(10, height, width)
	arr = renderVoronoi2(arr, seeds)
	for (const seed of seeds) { arr = fillCircel(arr, seed, 5, color.gray) }
	const uint8Arr = toByteArr(arr)
	await Promise.all([
		createPpmFile('main.ppm', uint8Arr, width, height),
		createBitmapFile('main.bmp', uint8Arr, width, height, 24)
	])
}
