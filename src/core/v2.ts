import { createBitmapFile } from '../utils/bitmap.js'
import { euclideanDistance, fillArr, fillCircel, genRndSeeds2, makeArr, toByteArr } from '../utils/index.js'
import { createPpmFile } from '../utils/ppm.js'
import { IPoint } from './model.js'
import { color } from './palette.js'

function pointToColor(p: IPoint) {
	const dv = new DataView(new ArrayBuffer(4))
	dv.setUint16(0, p.x)
	dv.setUint16(2, p.y)
	return dv.getUint32(0)
}
function colorToPoint(c: number) {
	const dv = new DataView(new ArrayBuffer(4))
	dv.setUint32(0, c)
	return { x: dv.getUint16(0), y: dv.getUint16(2) }
}
function applyNextSeedColor(arr: Uint32Array[], p: IPoint) {
	const width = arr[0].length
	const height = arr.length
	const c = pointToColor(p)
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			const currSeed = colorToPoint(arr[y][x])
			if (euclideanDistance(p, { x, y }) > euclideanDistance(currSeed, { x, y })) { continue }
			arr[y][x] = c
		}
	}
	return arr
}
function renderVoronoi2(arr: Uint32Array[], seeds: IPoint[]) {
	const c = pointToColor(seeds[0])
	fillArr(arr, c)
	for (let i = 1; i < seeds.length; i++) { applyNextSeedColor(arr, seeds[i]) }
	return arr
}
export async function main() {
	const width = 800
	const height = 600
	const seedCount = 20
	let arr = makeArr(height, width)
	const seeds = genRndSeeds2(seedCount, height, width)
	arr = renderVoronoi2(arr, seeds)
	for (const seed of seeds) { arr = fillCircel(arr, seed, 5, color.gray) }
	// arr = renderGradient(arr)
	const uint8Arr = toByteArr(arr)
	await Promise.all([
		createPpmFile('2.ppm', uint8Arr, width, height),
		createBitmapFile('2.bmp', uint8Arr, width, height, 24)
	])
}

await main()