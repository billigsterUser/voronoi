[![pipeline status][PipelineStatusBadge]][ProjectUrl]
[![coverage report][CoverageBadge]][ProjectUrl]

# voronoi

### Chebyshev Distance Voronoi diagram
![Image](/seeded-Rnd-chebyshev-Distance.png?raw=true "Chebyshev Distance Voronoi diagram")
### Minkowski Distance Voronoi diagram
![Image](/seeded-Rnd-minkowski-Distance.png?raw=true "Minkowski Distance Voronoi diagram")
### Manhattan Distance Voronoi diagram
![Image](/seeded-Rnd-manhattan-Distance.png?raw=true "Manhattan Distance Voronoi diagram")
### Euclidean Distance Voronoi diagram
![Image](/seeded-Rnd-euclidean-Distance.png?raw=true "Euclidean Distance Voronoi diagram")




## [Wiki](https://billigsterUser.gitlab.io/voronoi/)

#### References:
- Wikipedia Article: https://en.wikipedia.org/wiki/Voronoi_diagram
- PPM Image Format: https://en.wikipedia.org/wiki/Netpbm
- Bitmap Image Format: https://en.wikipedia.org/wiki/Bitmap

## Getting started


## Installing from source
### Linux, MacOS, Windows, *BSD, Solaris, WSL, Android, Raspbian

```bash
git clone https://gitlab.com/billigsterUser/voronoi.git
cd voronoi
npm i
nm test

```

### Usage

```bash
npm run run
```

That's it!




[PipelineStatusBadge]: https://gitlab.com/billigsterUser/voronoi/badges/main/pipeline.svg
[CoverageBadge]: https://gitlab.com/billigsterUser/voronoi/badges/main/coverage.svg
[ProjectUrl]: https://gitlab.com/billigsterUser/voronoi/commits/main
[ProjectPage]: https://billigsterUser.gitlab.io/voronoi/